package agera01.poca.club.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private Button button;

    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.btn_alert);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMyDialog();
            }
        });

    }

    private void showMyDialog(){
        View view=View.inflate(this,R.layout.dialog_cancel_indent,null);
        dialog = new MyDialog(this, 200, 100, view, R.style.dialog);
        dialog.show();
        final TextView cancel =
                (TextView) view.findViewById(R.id.cancel);
        final TextView confirm =
                (TextView)view.findViewById(R.id.confirm);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "点击了取消按钮", Toast.LENGTH_SHORT).show();
                Log.d("ddh", "onClick: " + cancel);
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "点击了确认按钮", Toast.LENGTH_SHORT).show();
                Log.d("ddh", "onClick: " + confirm);
                dialog.dismiss();
            }
        });
    }

}
